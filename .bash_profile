# Common includes

# Alias

# Manual loading
# source ~/.include/.aliases/.shell
# source ~/.include/.aliases/.directories
# source ~/.include/.aliases/.software
# source ~/.include/.aliases/.git

# Autoloading
shopt -s dotglob
ALIASES_DIR=~/.include/.aliases/*
for f in $ALIASES_DIR; do source $f; done
shopt -u dotglob

# Manual loading
# Variables and paths
# source ~/.include/.variables/.composer
# source ~/.include/.variables/.golang
# source ~/.include/.variables/.virtualenvwrapper

# Autoloading
shopt -s dotglob
VARIABLES_AND_PATH_DIR=~/.include/.variables/*
for f in $VARIABLES_AND_PATH_DIR; do source $f; done
shopt -u dotglob


# .bash_profile

# Bash prompt
source ~/.bash_colors

# Path
export PATH="$PATH:$HOME/bin"

# Variables

# Bash completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
   . $(brew --prefix)/etc/bash_completion
fi

# Rbenv
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
